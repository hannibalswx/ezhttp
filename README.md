重点更新
==========
* PHP最高可安装PHP7.1.6，默认安装PgSQL支持
* 已测试服务器：阿里云 CentOS-7.2(64位)
* [查看PHPINFO](http://info.ctolog.com)
* 提示：php 7.1 发现有许多不兼容的，建议使用php 7.0

项目描述
===========
本项目是[EZHTTP](https://github.com/centos-bz/EZHTTP)分支，提供最新稳定的版本支持。
ezhttp是Web服务器安装一个强大的bash脚本（Apache，Nginx，PHP，MySQL等），你可以在一个非常简单的方法安装Apache Nginx PHP MySQL，只需要输入数字来选择你想安装安装前，所有的事情都会在几分钟内完成。


支持系统
===============
* CentOS-5.x
* CentOS-6.x
* CentOS-7.x
* Ubuntu-12.04
* Ubuntu-14.04
* Debian-6.x
* Debian-7.x
* Other Linux Distribution

支持软件
==================
* Nginx OpenResty Tengine
* Nginx Module: lua_nginx_module nginx_concat_module nginx_upload_module ngx_substitutions_filter_module
* Apache-2.2 Apache-2.4
* MySQL-5.1 MySQL-5.5 MySQL-5.6 MySQL-5.7
* PHP-5.2 PHP-5.3 PHP-5.4 PHP-5.5 PHP-5.6 PHP-7.0 PHP-7.1
* PHP Module: ZendOptimizer ZendGuardLoader Xcache Eaccelerator Imagemagick IonCube Memcache Memcached Redis Mongo Xdebug Mssql PgSQL
* 其它软件: Memcached PureFtpd PhpMyAdmin Redis Mongodb PhpRedisAdmin MemAdmin RockMongo Jdk7 Jdk8 Tomcat7 Tomcat8

辅助工具
============
* System_swap_settings - 设置自动交换文件
* Generate_mysql_my_cnf - 生成mysql my.cnf配置文件
* 您的服务器内存和其他选项您设置。
* Create_rpm_package - 软件打包成RPM的工具
* Percona_xtrabackup_install - 安装时需要帮助的XtraBackup，备份MySQL数据。
* Change_sshd_port - 改变服务器的sshd端口
* Iptables_settings - 简单的设置iptables
* Enable_disable_php_extension - 管理PHP扩展模块
* Set_timezone_and_sync_time - 设置系统时区和NTP时间同步
* Initialize_mysql_server - MySQL服务器数据初始化
* Add_chroot_shell_user - 创建一个chroot目录Linux系统用户
* Network_analysis - 网络状态工具
* Backup_setup - 功能强大的备份工具.

安装
============
```bash
wget  http://zoujingli.oschina.io/ezhttp/master.zip?time=$(date +%s) -O ezhttp.zip
unzip ezhttp.zip
cd ezhttp
chmod +x start.sh
./start.sh
```

默认位置
=============================
| Nginx Location             |                                  |
|----------------------------|----------------------------------|
| Install Prefix             | /usr/local/nginx                 |
| Main Configuration File    | /usr/local/nginx/conf/nginx.conf |
| Virtual Host Configuration | /usr/local/nginx/conf/vhost/     |

| Apache Location            |                                   |
|----------------------------|-----------------------------------|
| Install Prefix             | /usr/local/apache                 |
| Main Configuration File    | /usr/local/apache/conf/httpd.conf |
| Virtual Host Configuration | /usr/local/nginx/conf/vhost/      |

| PHP Location           |                               |
|------------------------|-------------------------------|
| Install Prefix         | /usr/local/php                |
| Ini Configuration File | /usr/local/php/etc/php.ini    |
| FPM Configuration File | /usr/local/apache/conf/vhost/ |

| MySQL Location            |                                       |
|---------------------------|---------------------------------------|
| Install Prefix            | /usr/local/mysql                      |
| Data Location             | /usr/local/mysql/data                 |
| my.cnf Configuration File | /usr/local/mysql/etc/my.cnf           |
| Error Log Location        | /usr/local/mysql/data/mysql-error.log |
| Slow Log Location         | /usr/local/mysql/data/mysql-slow.log  |

进程管理
==================
| 进程 | 命令                                  |
|---------|------------------------------------------|
| nginx   | /etc/init.d/nginx (start/stop/restart)   |
| apache  | /etc/init.d/httpd (start/stop/restart)   |
| php-fpm | /etc/init.d/php-fpm (start/stop/restart) |
| mysql   | /etc/init.d/mysqld (start/stop/restart)  |

ez命令使用
=======================
| 命令 | 描述                                  |
|---------|------------------------------------------|
| ez vhost add   | create virtual host   |
| ez vhost list  | list all virtual host   |
| ez vhost del | remove a virtual host |
| ez mysql reset  | reset mysql password  |
| ez mysql add  | create a mysql user   |

错误和问题反馈
=============
请随时向作者报告任何错误和问题，作者的电子邮件是：
zoujingli@qq.com.
中文支持:https://www.lxconfig.com/forum-65-1.html
